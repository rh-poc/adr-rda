RDA Proof of Concept
==============================================

Process
----------------------------------------------

### RDA Main process

![process svg](src/main/resources/com/adr/rda/rda.RDA-svg.svg)

### RDA Autorizzazione

![process svg](src/main/resources/com/adr/rda/rda.RDA-autorizzazione-subproc-svg.svg)

Implementation decisions
----------------------------------------------

1. "Rilascio RDA - ROC": prende solo visione della RDA, non c'è nessuna azione.
2. Fase 5-6 nel documento si distingue tra "Approvazione" "Rilascio al sistema", siccome entrambe le azioni sono svolte dal CEI, sono state riunite in unico task: "Verifica RDA"