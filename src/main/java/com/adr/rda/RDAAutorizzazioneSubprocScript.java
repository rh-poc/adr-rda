package com.adr.rda;

import org.kie.api.runtime.process.ProcessContext;

/**
 * RDAAutorizzazioneSubprocScript
 */
public class RDAAutorizzazioneSubprocScript {

    // com.adr.rda.RDAAutorizzazioneSubprocScript.decisionOnEntry(kcontext);
    public static void decisionOnEntry(ProcessContext kcontext) {
        Long amount = (Long) kcontext.getVariable("amount");
        if (amount == null)
            amount = 0L;

        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            for (Posizione pos : rda.getPosizioni()) {
                amount += pos.getPrezzoTotale();
            }
        }

        kcontext.setVariable("amount", amount);
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.programmazioneOnEntry(kcontext);
    public static void programmazioneOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("verifica conformità dati");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.responsabileSUOnEntry(kcontext);
    public static void responsabileSUOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("verifica e approva (livello 2)");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.responsabileUOOnEntry(kcontext);
    public static void responsabileUOOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("verifica e approva (livello 3)");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.direttoreOnEntry(kcontext);
    public static void direttoreOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("verifica e approva (livello 4)");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.pgcOnEntry(kcontext);
    public static void pgcOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("verifica controller (PGC)");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.adOnEntry(kcontext);
    public static void adOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("approvazione AD");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.cdaOnEntry(kcontext);
    public static void cdaOnEntry(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            rda.setStatoRichiesta("approvazione CDA");
            kcontext.setVariable("rda", rda);
        }
    }

    // com.adr.rda.RDAAutorizzazioneSubprocScript.aggiornaStatoRichiesta(kcontext);
    public static void aggiornaStatoRichiesta(ProcessContext kcontext) {
        RDA rda = (RDA) kcontext.getVariable("rda");
        if (rda != null) {
            Boolean autorizzazione = (Boolean) kcontext.getVariable("autorizzazione");
            if (autorizzazione == true)
                rda.setStatoRichiesta("approvata");
            else
                rda.setStatoRichiesta("rifiutata");

            kcontext.setVariable("rda", rda);
        }
    }

}