package com.adr.rda;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.jbpm.document.service.impl.DocumentImpl;

/**
 * Documenti
 */
@Embeddable
public class Documenti implements Serializable {

    private static final long serialVersionUID = 1L;

    private DocumentImpl notaAccompagnatoria;
    private DocumentImpl listinoPrezzi;
    private DocumentImpl capitolatoTecnico;

    @Override
    public String toString() {
        return String.format("Documenti {capitolatoTecnico: %s, listinoPrezzi: %s, notaAccompagnatoria: %s }",
                capitolatoTecnico, listinoPrezzi, notaAccompagnatoria);
    }

    // ************************************************************
    // GETTERS / SETTERS
    // ************************************************************

    /**
     * @return the notaAccompagnatoria
     */
    public DocumentImpl getNotaAccompagnatoria() {
        return notaAccompagnatoria;
    }

    /**
     * @param notaAccompagnatoria the notaAccompagnatoria to set
     */
    public void setNotaAccompagnatoria(DocumentImpl notaAccompagnatoria) {
        this.notaAccompagnatoria = notaAccompagnatoria;
    }

    /**
     * @return the listinoPrezzi
     */
    public DocumentImpl getListinoPrezzi() {
        return listinoPrezzi;
    }

    /**
     * @param listinoPrezzi the listinoPrezzi to set
     */
    public void setListinoPrezzi(DocumentImpl listinoPrezzi) {
        this.listinoPrezzi = listinoPrezzi;
    }

    /**
     * @return the capitolatoTecnico
     */
    public DocumentImpl getCapitolatoTecnico() {
        return capitolatoTecnico;
    }

    /**
     * @param capitolatoTecnico the capitolatoTecnico to set
     */
    public void setCapitolatoTecnico(DocumentImpl capitolatoTecnico) {
        this.capitolatoTecnico = capitolatoTecnico;
    }
}