package com.adr.rda;

import org.jbpm.document.service.impl.DocumentCollectionImpl;
import org.kie.api.runtime.process.ProcessContext;

/**
 * RDAProcScript
 */
public class RDAProcScript {

    public static void rilascioRDAOnEntry(ProcessContext kcontext) {
        // on entry
        // com.adr.rda.RDAProcScript.rilascioRDAOnEntry(kcontext);
        RDA rda = (RDA) kcontext.getVariable("rda");
        System.out.println("rda docs " + rda.getDocumenti());
        DocumentCollectionImpl docs = new DocumentCollectionImpl();
        if (rda.getDocumenti().getNotaAccompagnatoria() != null)
            docs.addDocument(rda.getDocumenti().getNotaAccompagnatoria());
        if (rda.getDocumenti().getCapitolatoTecnico() != null)
            docs.addDocument(rda.getDocumenti().getCapitolatoTecnico());
        if (rda.getDocumenti().getListinoPrezzi() != null)
            docs.addDocument(rda.getDocumenti().getListinoPrezzi());
        kcontext.setVariable("docs", docs);
    }
}