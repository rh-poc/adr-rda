package com.adr.rda;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.kie.api.definition.type.Label;

/**
 * RegimeFiscale
 */
@Entity
public class RegimeFiscale implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO, generator = "REGIMEFISCALE_ID_GENERATOR")
	@Id
	@SequenceGenerator(sequenceName = "REGIMEFISCALE_ID_SEQ", name = "REGIMEFISCALE_ID_GENERATOR")
	private Long id;

    // Tipologia rapporto: descrive il tipo di rapporto tra la società acquirente e il fornitore
    @Label("tipologia rapporto")
    private String tipologiaRapporto;
    // Luogo di esecuzione: luogo da dove viene eseguito il servizio, scegliere nel menù a tendina: da remoto, in loco
    @Label("luogo di esecuzione")
    private String luogoEsecuzione;
    // Area di intervento: area nel quale viene effettuato il servizio
    @Label("area di intervento")
    private String areaIntervento;
    // Finalità del servizio
    @Label("finalità del servizio")
    private String finalitaServizio;
    // Destinatario servizio
    @Label("destinatario servizio")
    private String destinatarioServizio;
    // Rilascio: data di presunto rilascio dell’ RdA
    @Label("rilascio")
    private LocalDate rilascio;
    // Compilatore: nome dell’utente richiedente
    @Label("compilatore")
    private String compilatore;
    // Fornitore: nome del fornitore del bene/servizio
    @Label("fornitore")
    private String fornitore;
    // Email: Email dell’utente richiedente
    @Label("email")
    private String email;
    // Telefono: numero di telefono della struttura di appartenenza dell’utente richiedente
    @Label("telefono")
    private String tel;
    // Descrizione Lavori: breve descrizione dell’acquisto ( quella già inserita nello step 1)
    @Label("descrizione lavori")
    private String descrizioneLavori;

    @Override
    public String toString() {
        return String.format(
                "RegimeFiscale {areaIntervento: %s, compilatore: %s, descrizioneLavori: %s, destinatarioServizio: %s, email: %s, finalitaServizio: %s, fornitore: %s, luogoEsecuzione: %s, rilascio: %s, tel: %s, tipologiaRapporto: %s }",
                areaIntervento, compilatore, descrizioneLavori, destinatarioServizio, email, finalitaServizio,
                fornitore, luogoEsecuzione, rilascio, tel, tipologiaRapporto);
    }

   	// ************************************************************
	// GETTERS / SETTERS
	// ************************************************************

    /**
     * @return the tipologiaRapporto
     */
    public String getTipologiaRapporto() {
        return tipologiaRapporto;
    }

    /**
     * @param tipologiaRapporto the tipologiaRapporto to set
     */
    public void setTipologiaRapporto(String tipologiaRapporto) {
        this.tipologiaRapporto = tipologiaRapporto;
    }

    /**
     * @return the luogoEsecuzione
     */
    public String getLuogoEsecuzione() {
        return luogoEsecuzione;
    }

    /**
     * @param luogoEsecuzione the luogoEsecuzione to set
     */
    public void setLuogoEsecuzione(String luogoEsecuzione) {
        this.luogoEsecuzione = luogoEsecuzione;
    }

    /**
     * @return the areaIntervento
     */
    public String getAreaIntervento() {
        return areaIntervento;
    }

    /**
     * @param areaIntervento the areaIntervento to set
     */
    public void setAreaIntervento(String areaIntervento) {
        this.areaIntervento = areaIntervento;
    }

    /**
     * @return the finalitaServizio
     */
    public String getFinalitaServizio() {
        return finalitaServizio;
    }

    /**
     * @param finalitaServizio the finalitaServizio to set
     */
    public void setFinalitaServizio(String finalitaServizio) {
        this.finalitaServizio = finalitaServizio;
    }

    /**
     * @return the destinatarioServizio
     */
    public String getDestinatarioServizio() {
        return destinatarioServizio;
    }

    /**
     * @param destinatarioServizio the destinatarioServizio to set
     */
    public void setDestinatarioServizio(String destinatarioServizio) {
        this.destinatarioServizio = destinatarioServizio;
    }

    /**
     * @return the rilascio
     */
    public LocalDate getRilascio() {
        return rilascio;
    }

    /**
     * @param rilascio the rilascio to set
     */
    public void setRilascio(LocalDate rilascio) {
        this.rilascio = rilascio;
    }

    /**
     * @return the compilatore
     */
    public String getCompilatore() {
        return compilatore;
    }

    /**
     * @param compilatore the compilatore to set
     */
    public void setCompilatore(String compilatore) {
        this.compilatore = compilatore;
    }

    /**
     * @return the fornitore
     */
    public String getFornitore() {
        return fornitore;
    }

    /**
     * @param fornitore the fornitore to set
     */
    public void setFornitore(String fornitore) {
        this.fornitore = fornitore;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the descrizioneLavori
     */
    public String getDescrizioneLavori() {
        return descrizioneLavori;
    }

    /**
     * @param descrizioneLavori the descrizioneLavori to set
     */
    public void setDescrizioneLavori(String descrizioneLavori) {
        this.descrizioneLavori = descrizioneLavori;
    }
}