package com.sap;

import com.adr.rda.RDA;

import org.kie.api.runtime.process.ProcessWorkItemHandlerException;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.api.runtime.process.ProcessWorkItemHandlerException.HandlingStrategy;

/**
 * WorkItemHandlerSAP
 */
public class SAPWorkItemHandler implements WorkItemHandler {
    private String processId;
    private HandlingStrategy strategy;
    
    public SAPWorkItemHandler(String processId, HandlingStrategy strategy) {
        super();
        this.processId = processId;
        this.strategy = strategy;
    }
    
    public SAPWorkItemHandler(String processId, String strategy) {
        super();
        this.processId = processId;
        this.strategy = HandlingStrategy.valueOf(strategy);
    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        String message = "WIH for SAP execute (";
        for ( String k : workItem.getParameters().keySet()) {
            message += String.format("%s = %s, ",k, workItem.getParameter(k));
        };
        System.out.println(message + ")");

        RDA rda = (RDA) workItem.getParameter("rda");
        if (processId != null && strategy != null && rda != null && rda.getTitolo().contains("error")) {
            throw new ProcessWorkItemHandlerException(processId, strategy, new RuntimeException("On purpose"));
        }

        manager.completeWorkItem(workItem.getId(), null);
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        System.out.println("WIH for SAP abort");
	}

    
}